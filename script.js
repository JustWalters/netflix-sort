jQuery(function ($) {
	/*TODO: serious refactoring, maybe sort on something else, test sort alls*/
	var allBtn = $("<input type='button' style='margin-right:10px' value='Sort All (together)' />");
	allBtn.click(sortAll);
	$("#queue h2, #qbr").append(allBtn);

	var bothBtn = $("<input type='button' style='margin-right:10px' value='Sort All (separate)' />");
	bothBtn.click(sortBoth);
	$("#queue h2, #qbr").append(bothBtn);

	var justTvBtn = $("<input type='button' style='margin-right:10px' value='Sort Only TV' />");
	justTvBtn.click(sortTv);
	$("#queue h2, #qbr").append(justTvBtn);

	var justFilmBtn = $("<input type='button' style='margin-right:10px' value='Sort Only Movies' />");
	justFilmBtn.click(sortFilms);
	$("#queue h2, #qbr").append(justFilmBtn);


	var films;
	var totRows;


	function sortFilms() {
		totRows = 0;
		films = $("#qbody tr").map(getFilmInfo);
		films.sort(compareFilms);
		films.each(inputOrder);
	}

	function sortTv() {
		totRows = 0;
		films = $("#qbody tr").map(getTvInfo);
		films.sort(compareFilms);
		films.each(inputOrderTv);
	}

	function sortAll() {
		totRows = 0;
		films = $("#qbody tr").map(getAllInfo);
		films.sort(compareFilms);
		films.each(inputOrder);
	}

	function sortBoth() {
		sortFilms();
		sortTv();
	}

	function getFilmInfo(id, el) {
		var row = $(el);
		var gen = row.find(".genre").text();
		totRows++;
		if (gen === "TV Shows") {
			return null;
		} else {
			return {
				row: row,
				genre: gen,
				rating: row.find(".sbmfpr, .sbmfrt").attr("class").match(/sbmf-([0-9]*)/)[1]
			};
		}
	}

	function getTvInfo(id, el) {
		var row = $(el);
		var gen = row.find(".genre").text();
		totRows++;
		if (gen === "TV Shows") {
			return {
				row: row,
				genre: gen,
				rating: row.find(".sbmfpr, .sbmfrt").attr("class").match(/sbmf-([0-9]*)/)[1]
			};
					} else {
			return null;
		}
	}

	function getAllInfo(id, el) {
		var row = $(el);
		var gen = row.find(".genre").text();
		totRows++;
		return {
				row: row,
				genre: gen,
				rating: row.find(".sbmfpr, .sbmfrt").attr("class").match(/sbmf-([0-9]*)/)[1]
			};
		}

	function compareFilms(a, b) {
		return (b.rating - a.rating) || (b.genre - a.genre);
	}

	function inputOrder(id, el) {
		el.row.find(".o").val(id + 1);
	}

	function inputOrderTv(id, el) {
		el.row.find(".o").val(totRows - id);
	}
});
